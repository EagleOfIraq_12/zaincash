package com.morabaa.team.zaincash;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class ConfirmActivity extends AppCompatActivity {
      
      String newUrl;
      WebView webView;
      String packageName;
      String className;
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_confirm);
            newUrl = getIntent().getExtras().getString("newUrl");
            packageName = getIntent().getExtras().getString("packageName");
            className = getIntent().getExtras().getString("className");
            webView = findViewById(R.id.webView);
            webView.loadUrl(newUrl);
            webView.setWebViewClient(new WebViewClient() {
                  @Override
                  public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                        System.out.println("url: " + url);
                        
                        if (url.contains("https://morabaa.com")) {
                              Intent intent = getPackageManager()
                                    .getLaunchIntentForPackage(packageName);
                              if (getPackageManager().resolveActivity(intent, 0) != null) {
                                    assert intent != null;
                                    intent.setAction("android.intent.action.ZAIN_CASH_PAY");
                                    intent.setComponent(
                                          new ComponentName(packageName, className)
                                    );
      
                                    Uri uri = Uri.parse(url);
      
                                    String token = uri.getQueryParameter("token");
                                    System.out.println("token: " + token);
      
                                    intent.putExtra("key", token);
                                    startActivity(intent);
                                    finish();
                              } else {
                                    Toast.makeText(ConfirmActivity.this,
                                          "please install zainCash App",
                                          Toast.LENGTH_SHORT).show();
                                    final String appPackageName = "mobi.foo.zaincash"; // package name of the app
                                    try {
                                          startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
                                                "market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                          startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
                                                "https://play.google.com/store/apps/details?id="
                                                      + appPackageName)));
                                    }
                              }
                        }
                        Log.d("WebView", "your current url when webpage loading.." + url);
                  }
                  
                  @Override
                  public void onPageFinished(WebView view, String url) {
                        Log.d("WebView", "your current url when webpage loading.. finish" + url);
                        super.onPageFinished(view, url);
                  }
                  
                  @Override
                  public void onLoadResource(WebView view, String url) {
                        // TODO Auto-generated method stub
                        super.onLoadResource(view, url);
                  }
                  
                  @Override
                  public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        System.out.println(
                              "when you click on any interlink on webview that time you got url :-"
                                    + url);
                        return super.shouldOverrideUrlLoading(view, url);
                  }
            });
      }
      
      @Override
      public void onBackPressed() {
            
            if (webView.canGoBack()) {
                  webView.goBack();
            } else {
                  super.onBackPressed();
            }
            
      }
      
}
