package com.morabaa.team.zaincash;

import android.content.Context;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;

/**
 * Created by eagle on 2/10/2018.
 */

public abstract class HttpRequest {
      
      private static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
      Context ctx;
      String url;
      String jsonBodyString;
      HashMap<String, String> headers = new HashMap<>();
      Map<String, String> parms;
      private OkHttpClient client = new OkHttpClient();
      
      public HttpRequest(String url, String jsonBodyString,
            Map<String, String> parms) {
            this.ctx = ctx;
            this.url = url;
            this.jsonBodyString = jsonBodyString;
            this.parms = parms;
            postToServer();
            
      }
      
      private void postToServer() {
            new Thread(
                  new Runnable() {
                        @Override
                        public void run() {
                              okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();
                              okhttp3.Request.Builder builder = new okhttp3.Request.Builder()
                                    .url(url);
                              if (parms != null) {
                                    okhttp3.FormBody.Builder postData = new okhttp3.FormBody.Builder();
                                    for (String key : parms.keySet()) {
                                          postData.add(key, parms.get(key));
                                    }
                                    builder.post(postData.build());
                              }
                              okhttp3.Request request = builder.build();
                              okhttp3.Response response;
                              try {
                                    response = client.newCall(request).execute();
                                    if (!response.isSuccessful()) {
                                          onError(response.message() + " " + response.toString());
                                    } else {
                                          if (response.body() != null) {
                                                onFinish(response.body().string());
                                          }
                                    }
                              } catch (IOException e) {
                                    e.printStackTrace();
                              }
                        }
                  }
            ).start();
      }
      
      public abstract void onFinish(String response);
      
      public abstract void onError(String error);
      
}
