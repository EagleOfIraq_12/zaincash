package com.morabaa.team.zaincash;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
      
      long amount = 0;
      String secret = "";
      long msisdn = 0;
      int orderId = 0;
      String redirectUrl;
      String serviceType = "";
      String merchantId = "";
      String packageName = "";
      String className = "";
      
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            try {
                  Intent intent = getIntent();
                  amount = Objects.requireNonNull(intent.getExtras()).getLong("amount");
                  secret = intent.getExtras().getString("secret");
                  msisdn = intent.getExtras().getLong("msisdn");
                  orderId = intent.getExtras().getInt("orderId");
                  redirectUrl = intent.getExtras().getString("redirectUrl");
                  serviceType = intent.getExtras().getString("serviceType");
                  merchantId = intent.getExtras().getString("merchantId");
                  packageName = intent.getExtras().getString("packageName");
                  className = intent.getExtras().getString("className");
                  pay();
                  
            } catch (Exception e) {
                  System.out.print("Error: "+e.getMessage());
            }
      }
      
      public String generateJWT(Map<String, Object> data) {
//            String secret = "$2y$10$wYDj3ZTfWAEQ/2EmyRMRj.RA3huRb3FkO5gJeHrFeasgtgY9xEBmi";
            
            secret = new String(Base64.encodeBase64(secret.getBytes()));
            
            return Jwts.builder()
                  .addClaims(data)
                  .signWith(SignatureAlgorithm.HS256, secret)
                  .compact();
      }
      
      public void pay() {
//            merchantId = "5ab9135fc125b3f929317520";// ((EditText) findViewById(R.id.txtMerchantId)).getText()
            Map<String, Object> data = new HashMap<>();
            
            data.put("amount", amount);
            data.put("msisdn", msisdn);
            data.put("orderId", orderId);
            data.put("redirectUrl", redirectUrl);
            data.put("iat", new Date().getTime());
            data.put("exp", new Date().getTime() + 60 * 60 * 4);
            data.put("serviceType", serviceType);
            
            String newToken = generateJWT(data);
//                        String newToken = JWTEncoder.encode(data).replace("\n","");
            System.out.println("newToken: " + newToken);
            
            Map<String, String> parms = new HashMap<>();
            
            parms.put("token", newToken);
            parms.put("merchantId", merchantId);
            parms.put("lang", "ar");
            
            new HttpRequest(
                  "https://api.zaincash.iq/transaction/init", "", parms) {
                  @Override
                  public void onFinish(String response) {
                        System.out.println("response: " + response);
                        String id;
                        try {
                              JSONObject object = new JSONObject(response);
                              id = object.getString("id");
                              String newUrl =
                                    "https://api.zaincash.iq/transaction/pay?id=" + id;
                              Intent intent =
                                    new Intent(MainActivity.this, ConfirmActivity.class);
                              intent.putExtra("newUrl", newUrl);
                              intent.putExtra("packageName", packageName);
                              intent.putExtra("className", className);
                              startActivity(intent);
                              finish();
                        } catch (JSONException e) {
                              Intent intent = getPackageManager()
                                    .getLaunchIntentForPackage(packageName);
                              if (getPackageManager().resolveActivity(intent, 0) != null) {
                                    assert intent != null;
                                    intent.setAction("android.intent.action.ZAIN_CASH_PAY");
                                    intent.setComponent(
                                          new ComponentName(packageName, className)
                                    );
                                    startActivity(intent);
                                    finish();
                              }
                              
                        }
                  }
                  
                  @Override
                  public void onError(String error) {
                        System.out.println("Error: " + error);
                  }
            };
      }
}
